/**
 * Adobe Edge: symbol definitions
 */
(function($, Edge, compId){
//images folder
var im='images/';

var fonts = {};
var opts = {};
var resources = [
];
var symbols = {
"stage": {
    version: "3.0.0",
    minimumCompatibleVersion: "3.0.0",
    build: "3.0.0.322",
    baseState: "Base State",
    scaleToFit: "none",
    centerStage: "none",
    initialState: "Base State",
    gpuAccelerate: false,
    resizeInstances: false,
    content: {
            dom: [
            {
                id: 'Ellipse',
                type: 'ellipse',
                rect: ['167px', '92px','215px','215px','auto', 'auto'],
                borderRadius: ["50%", "50%", "50%", "50%"],
                fill: ["rgba(192,192,192,1)"],
                stroke: [0,"rgba(0,0,0,1)","none"]
            },
            {
                id: 'discs',
                type: 'rect',
                rect: ['141', '63','auto','auto','auto', 'auto']
            }],
            symbolInstances: [
            {
                id: 'discs',
                symbolName: 'discs'
            }
            ]
        },
    states: {
        "Base State": {
            "${_Ellipse}": [
                ["style", "left", '167px'],
                ["style", "top", '92px']
            ],
            "${_Stage}": [
                ["color", "background-color", 'rgba(255,255,255,1)'],
                ["style", "width", '550px'],
                ["style", "height", '400px'],
                ["style", "overflow", 'hidden']
            ]
        }
    },
    timelines: {
        "Default Timeline": {
            fromState: "Base State",
            toState: "",
            duration: 0,
            autoPlay: true,
            timeline: [
            ]
        }
    }
},
"discs": {
    version: "3.0.0",
    minimumCompatibleVersion: "3.0.0",
    build: "3.0.0.322",
    baseState: "Base State",
    scaleToFit: "none",
    centerStage: "none",
    initialState: "Base State",
    gpuAccelerate: false,
    resizeInstances: false,
    content: {
            dom: [
                {
                    id: 'colordots',
                    type: 'group',
                    rect: ['0', '0', '267', '271', 'auto', 'auto'],
                    c: [
                    {
                        rect: ['123px', '0px', '21px', '21px', 'auto', 'auto'],
                        borderRadius: ['50%', '50%', '50%', '50%'],
                        id: 'red',
                        stroke: [0, 'rgb(0, 0, 0)', 'none'],
                        type: 'ellipse',
                        fill: ['rgba(249,0,0,1.00)']
                    },
                    {
                        rect: ['123px', '250px', '21px', '21px', 'auto', 'auto'],
                        borderRadius: ['50%', '50%', '50%', '50%'],
                        id: 'yellow',
                        stroke: [0, 'rgb(0, 0, 0)', 'none'],
                        type: 'ellipse',
                        fill: ['rgba(246,227,9,1.00)']
                    },
                    {
                        rect: ['246px', '126px', '21px', '21px', 'auto', 'auto'],
                        borderRadius: ['50%', '50%', '50%', '50%'],
                        id: 'green',
                        stroke: [0, 'rgb(0, 0, 0)', 'none'],
                        type: 'ellipse',
                        fill: ['rgba(67,255,0,1.00)']
                    },
                    {
                        rect: ['0px', '126px', '21px', '21px', 'auto', 'auto'],
                        borderRadius: ['50%', '50%', '50%', '50%'],
                        id: 'blue',
                        stroke: [0, 'rgb(0, 0, 0)', 'none'],
                        type: 'ellipse',
                        fill: ['rgba(7,25,246,1.00)']
                    }]
                }
            ],
            symbolInstances: [
            ]
        },
    states: {
        "Base State": {
            "${_green}": [
                ["style", "top", '126px'],
                ["style", "left", '246px'],
                ["color", "background-color", 'rgba(67,255,0,1.00)']
            ],
            "${_colordots}": [
                ["transform", "rotateZ", '0deg']
            ],
            "${symbolSelector}": [
                ["style", "height", '271px'],
                ["style", "width", '267px']
            ],
            "${_yellow}": [
                ["style", "top", '250px'],
                ["style", "left", '123px'],
                ["color", "background-color", 'rgba(246,227,9,1.00)']
            ],
            "${_blue}": [
                ["style", "top", '126px'],
                ["style", "left", '0px'],
                ["color", "background-color", 'rgba(7,25,246,1.00)']
            ],
            "${_red}": [
                ["style", "top", '0px'],
                ["style", "left", '123px'],
                ["color", "background-color", 'rgba(249,0,0,1.00)']
            ]
        }
    },
    timelines: {
        "Default Timeline": {
            fromState: "Base State",
            toState: "",
            duration: 1500,
            autoPlay: false,
            labels: {
                "redtop": 0,
                "bluetop": 500,
                "yelowtop": 1000,
                "greentop": 1500
            },
            timeline: [
            ]
        }
    }
}
};


Edge.registerCompositionDefn(compId, symbols, fonts, resources, opts);

/**
 * Adobe Edge DOM Ready Event Handler
 */
$(window).ready(function() {
     Edge.launchComposition(compId);
});
})(jQuery, AdobeEdge, "EDGE-1374010289");
