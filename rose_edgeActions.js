/***********************
* Adobe Edge Animate Composition Actions
*
* Edit this file with caution, being careful to preserve 
* function signatures and comments starting with 'Edge' to maintain the 
* ability to interact with these actions from within Adobe Edge Animate
*
***********************/
(function($, Edge, compId){
var Composition = Edge.Composition, Symbol = Edge.Symbol; // aliases for commonly used Edge classes

   //Edge symbol: 'stage'
   (function(symbolName) {
      
      
   })("stage");
   //Edge symbol end:'stage'

   //=========================================================
   
   //Edge symbol: 'discs'
   (function(symbolName) {   
   
      Symbol.bindElementAction(compId, symbolName, "${_blue}", "mouseover", function(sym, e) {
         // Gets an element. For example, 
         // var element = sym.$("Text2");
         // element.hide();
         var bluedot = sym.$("colordots");
         TweenMax.to(bluedot, 2, {css:{rotation:90}});

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_green}", "mouseover", function(sym, e) {
         var greendot = sym.$("colordots");
         TweenMax.to(greendot, 2, {css:{rotation:-90}});

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_yellow}", "mouseover", function(sym, e) {
         var yellowdot = sym.$("colordots");
         TweenMax.to(yellowdot, 2, {css:{rotation:180}});

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_red}", "mouseover", function(sym, e) {
         var reddot = sym.$("colordots");
         TweenMax.to(reddot, 2, {css:{rotation:0}});

      });
      //Edge binding end

   })("discs");
   //Edge symbol end:'discs'

})(jQuery, AdobeEdge, "EDGE-1374010289");